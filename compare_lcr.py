# %%
import os

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import pandas as pd
from scipy.stats import pearsonr
# %%
from utils import names


def average(l):
    return round(sum(l) / len(l), 2)


def moving_average(l, k=4):
    result = []
    for i in range(len(l)):
        this_sum = 0
        this_cnt = 0
        for j in range(-k, k):
            if 0 <= (i + j) < len(l):
                this_cnt += 1
                this_sum += l[i + j]
        result.append(this_sum / this_cnt)
    return result


# files = ['1mnist_lenet_gf0.01_fgsm.txt',
#          '1mnist_lenet_gf0.01_fgsm.txt'
#          '1mnist_lenet_gf0.01_fgsm.txt'
#          '1mnist_lenet_gf0.01_fgsm.txt'
#          '1mnist_lenet_gf0.01_fgsm.txt'
#          '1mnist_lenet_gf0.01_fgsm.txt']

results = []
files = os.listdir('text_result/')
# for config in configs:
plt.figure(figsize=(15, 8))
for file in files:
    if not file.startswith('1mnist'):
        continue
    name = file[:-4]
    # name = names.get_name(mutation=config['mutation'], attack_type=config['attack'])
    w = open('text_result/{}.txt'.format(name), 'r')
    lcr_list = [float(line) for line in w.readlines()]
    indices = np.arange(0, len(lcr_list))
    sns.lineplot(x=indices, y=lcr_list, label=name)
    results.append([name, len(lcr_list), lcr_list[-1], round(pearsonr(indices, lcr_list)[0], 2)])
plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
plt.tight_layout()
plt.savefig('pic_result/lcrs.png')
df = pd.DataFrame(results, columns=['experiment', '#', 'final_lcr', 'pearson'])
df.to_csv('text_result/all.csv', index=False)
