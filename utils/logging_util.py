import logging.config
import yaml
from pkg_resources import resource_stream

from configs import LOGGING_CONFIG_ADDRESS


def setup_logging():
    # with resource_stream('resources', 'logging.yaml') as f:
    #     logging.config.dictConfig(yaml.safe_load(f.read()))
    yaml.warnings({'YAMLLoadWarning': False})
    with open(LOGGING_CONFIG_ADDRESS, 'r') as f:
        logging.config.dictConfig(yaml.load(f))
