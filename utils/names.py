def get_name(dataset='mnist', model_name='lenet', mutation='gf0.01', attack_type='fgsm'):
    return '1{}_{}_{}_{}'.format(dataset, model_name, mutation, attack_type)
