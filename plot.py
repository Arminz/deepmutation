import numpy as np
import matplotlib.pyplot as plt
name = '1mnist_lenet_changeoperator_gf_fgsm_0.01'
w = open('scripts/{}.txt'.format(name), 'r')
l = w.readlines()
h = []
for line in l:
    h.append(100 * eval(line))
    # print(eval(line)-0.1)

#
# w = open('result/dm_cifar_conv12_bb.txt','r')
# l = w.readlines()
# a = []
# for line in l:
#   a.append(eval(line))
#   #print(eval(line)-0.1)
#
# w = open('result/dm_cifar_conv12_cw.txt','r')
# l = w.readlines()
# c = []
# for line in l:
#   c.append(eval(line))
#   #print(eval(line)-0.1)
# w = open('result/dm_cifar_conv12_deepfool.txt','r')
# l = w.readlines()
# d = []
# for line in l:
#   d.append(eval(line))
#   #print(eval(line)-0.1)
# w = open('result/dm_cifar_conv12_jsma.txt','r')
# l = w.readlines()
# e = []
# for line in l:
#   e.append(eval(line))
#   #print(eval(line)-0.1)
#
# w = open('result/dm_cifar_conv12_fgsm.txt','r')
# l = w.readlines()
# f= []
# for line in l:
#   f.append(eval(line))
#   #print(eval(line)-0.1)
#

b = []
for i in range(0, len(l)):
    b.append(i)
# print(len(a))
# print(len(b))
# plt.gca().set_color_cycle(['red','green','blue','yellow','black'])
plt.figure(figsize=(10, 6))
plt.scatter(b, h, s=5, color='red')
# plt.scatter(b,a,s = 5,color='red')
# plt.scatter(b,c,s = 5,color='green')
# plt.scatter(b,d,s = 5,color='blue')
# plt.scatter(b,e,s = 5,color='yellow')
plt.ylim(0, 40)
# plt.scatter(b,f,s = 5,color='black')
# plt.legend(['bb','cw','deepfool','jsma','fgsm'],loc = 'upper left')
plt.xlabel("Percentage of Adversarial")
plt.ylabel("Increment compared to Original Data")
plt.title(name)
plt.savefig('result/{}.png'.format(name))
