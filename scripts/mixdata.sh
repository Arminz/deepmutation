#!/usr/bin/env bash

#SBATCH --time=0-00:30:00

#base_path='/home/armin/PycharmProjects/deepmutation/'
base_path='/home/arminz/deepmutation/'
export PYTHONPATH=$PYTHONPATH:$base_path

exe_file=$base_path/mix_data.py

python3 $exe_file --base_path $base_path