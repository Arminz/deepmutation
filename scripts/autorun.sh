#!/usr/bin/env bash

base_path='/home/armin/PycharmProjects/deepmutation/'
#base_path='/home/arminz/deepmutation/'
export PYTHONPATH=$PYTHONPATH:$base_path

read -p 'attack_type:' attack_type #fgsm
read -p 'mutation:' mutation #gf0.01
echo 'foo'$attack_type $mutation
#attack_type='jsma'
#mutation='gf0.01'
mv ../text_result/1mnist_lenet_$mutation'_'$attack_type.txt ../text_result/dp/1mnist_lenet_$mutation'_'$attack_type.txt

#set mutatedModelsPath ../build-in-resource/mutated_models/mnist/lenet/gf/5e-2p/
#set test_result_folder ../lcr_auc-testing-results/mnist/lenet/gf/5e-2p/cw/
#set nrLcrPath ../build-in-resource/nr-lcr/mnsit/lenet/gf/5e-2p/nrLCR.npy
#for j in {0..9}; do
j=0
for i in {0..100}; do
  echo 'iteration' $i
# spawn /home/ict520c/Documents/icse2019/scripts/newname.sh
# expect {
#     "choice" { send -- "n\n" }
#     "datatype" { send -- "0\n" }
#     "device" { send -- "-1\n" }
#     "useTrainData" { send -- "False\n" }
#     "batchModelSize" { send -- "2\n" }
#     "mutatedModelsPath" { send -- "$mutatedModelPath\n" }
#     "testSamplesPath" { send -- "../build-in-resource/dataset/mnist/adversarial/cw/a$i\n" }
#     "seedModelName" { send -- "lenet\n" }
#     "test_result_folder" { send -- "$test_result_folder\n" }
#     "maxModelsUsed" { send -- "10\n" }
#     "choice" { send -- "y\n" }
#     "nrLcrPath" { send -- "$nrLcrPath\n" }
#     "char\n" { send -- "\n" }
# }
# expect eof
#        read choice
    echo n  > input.txt

#        read -p "dataType:" dataType
    echo 0 >> input.txt
#
#       read -p "device:"  device
    echo -1 >> input.txt

#        read -p "testType:" testType
    echo adv >> input.txt

#        read -p "useTrainData:" useTrainData
    echo False >> input.txt

#        read -p "batchModelSize:" batchModelSize
    echo 2 >> input.txt

#        read -p "mutatedModelsPath:" mutatedModelsPath
    echo $base_path/artifacts_eval/modelMutation/$mutation/lenet >> input.txt

#        read -p "testSamplesPath:" testSamplesPath
    echo $base_path/artifacts_eval/mixed_samples/$attack_type/r$j/a$i >> input.txt

#        read -p "seedModelName:" seedModelName
    echo lenet >> input.txt

#        read -p "test_result_folder:" test_result_folder
    echo $base_path/lcr_auc-testing-results/mnist/lenet/$mutation/5e-2p/$attack_type >> input.txt

#    read -p "result_path:" result_path
    echo $base_path/text_result/1mnist_lenet_$mutation'_'$attack_type'.txt'>> input.txt

#        read -p "maxModelsUsed:" maxModelsUsed
    echo 2 >> input.txt

    echo y >> input.txt
    echo $base_path/build-in-resource/nr-lcr/mnsit/lenet/gf/5e-2p/nrLCR.npy >> input.txt

    $base_path/scripts/lcr_acu_analysis.sh < input.txt
done