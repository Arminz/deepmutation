#!/usr/bin/zsh
#attack_type='jsma'
#mutation='gf0.01'

echo 'jsma' > input_grid.txt
echo 'gf0.04' >> input_grid.txt
./autorun.sh < input_grid.txt


echo 'jsma' > input_grid.txt
echo 'ns0.03' >> input_grid.txt
./autorun.sh < input_grid.txt

echo 'jsma' > input_grid.txt
echo 'ws0.03' >> input_grid.txt
./autorun.sh < input_grid.txt


echo 'deepfool' > input_grid.txt
echo 'gf0.03' >> input_grid.txt
./autorun.sh < input_grid.txt

echo 'deepfool' > input_grid.txt
echo 'gf0.02' >> input_grid.txt
./autorun.sh < input_grid.txt

echo 'deepfool' > input_grid.txt
echo 'gf0.04' >> input_grid.txt
./autorun.sh < input_grid.txt

echo 'deepfool' > input_grid.txt
echo 'gf0.03' >> input_grid.txt
./autorun.sh < input_grid.txt

echo 'fgsm' > input_grid.txt
echo 'nai0.04' >> input_grid.txt
./autorun.sh < input_grid.txt

echo 'fgsm' > input_grid.txt
echo 'ws0.04' >> input_grid.txt
./autorun.sh < input_grid.txt

echo 'fgsm' > input_grid.txt
echo 'ns0.04' >> input_grid.txt
./autorun.sh < input_grid.txt