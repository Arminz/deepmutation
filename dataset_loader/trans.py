import os

result_folder = './build-in-resource/pure/'
os.chdir(result_folder)

# root = 't10k-images-idx3-ubyte_folder/'
# # all_files = os.listdir(root)
# f = open('t10k-labels-idx1-ubyte.txt', 'r')

root = 'train-images-idx3-ubyte_folder/'
# all_files = os.listdir(root)
f = open('train-labels-idx1-ubyte.txt', 'r')

lines = f.readlines()
a = []
for line in lines:
    # a.append(line[40]) #t10k-images
    a.append(line[-2]) #t10k-labels
    # print(a[1])
    save_path = root
    file_prefix = 'fgsm'
i = 0
for i in range(len(a)):
    if i < 10:
        s = '0000'
    elif 10 <= i < 100:
        s = '000'
    elif 100 <= i < 1000:
        s = '00'
    elif 1000 <= i <10000:
        s = '0'
    else:
        s = ''
    src = os.path.join(root, s + str(i) + '.png')
    adv_path = os.path.join(save_path,
                            file_prefix + '_' + '1111' + str(i) + '_' + str(a[i]) + '_' + str(
                                a[i]) + '_.png')
    os.rename(src, adv_path)
