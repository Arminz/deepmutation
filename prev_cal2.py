import numpy as np
import matplotlib.pyplot as plt
w = open('scripts/1mnist_lenet_changeoperator_gf_fgsm_10_10.txt','r')
l = w.readlines()
a = []
i = 0
x = []
for i in range(0,101):
    a.append(0.0)
for line in l:
    x.append(eval(line)*100)
print(len(x))
count = 0
a = np.array(a,dtype = float)
x = np.array(x,dtype = float)

for i in range(0,len(x)):
    if (count==100 or count==0):
        count = 0
      
    else:
        a[count] = a[count]+x[i]-x[i-1]
    print("i:{0}  a[count]{1} x[i]{2}  count{3} \n".format(i,a[count+1],x[i],count))
    count = count + 1 

b = []

for i in range(1,101):
  b.append(i)
for i in range(1,101):
    a[i] = a[i]/30
b = np.array(b,dtype = float)
np.savetxt('result/add_calresults.txt',a[1:101],fmt='%.6f')
print(len(b))
plt.figure(figsize=(10,6))
plt.scatter(b,a[1:101],s = 5)
plt.xlabel("Percentage of Adversarial")
plt.ylabel("Increment compared to Original Data")
plt.title('sa_lenet_mnist_add')
plt.savefig('pic_result/sa_lenet_mnist_add.png')