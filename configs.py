import os
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
LOGGING_CONFIG_ADDRESS = os.path.join(BASE_DIR, './config/logging.yaml')
SOURCE_DATA_ADDRESS = os.path.join(BASE_DIR, './build-in-resource/dataset/{}/raw')
