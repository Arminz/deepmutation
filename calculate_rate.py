# %%
import os

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import pandas as pd
from scipy.stats import pearsonr
# %%
from utils import names


def average(l):
    return round(sum(l) / len(l), 2)


def moving_average(l, k=4):
    result = []
    for i in range(len(l)):
        this_sum = 0
        this_cnt = 0
        for j in range(-k, k):
            if 0 <= (i + j) < len(l):
                this_cnt += 1
                this_sum += l[i + j]
        result.append(this_sum / this_cnt)
    return result


# configs = [
#     {'mutation': 'gf0.01', 'attack': 'fgsm'},
#     {'mutation': 'gf0.02', 'attack': 'fgsm'},
#     {'mutation': 'gf0.03', 'attack': 'fgsm'},
#     {'mutation': 'gf0.04', 'attack': 'fgsm'},
#     {'mutation': 'gf0.05', 'attack': 'fgsm'},
#     {'mutation': 'gf0.02', 'attack': 'jsma'},
#     {'mutation': 'gf0.04', 'attack': 'jsma'},
#     {'mutation': 'gf0.02', 'attack': 'deepfool'},
#     {'mutation': 'gf0.02', 'attack': 'cw'},
#     {'mutation': 'ws0.01', 'attack': 'fgsm'},
#     {'mutation': 'ws0.02', 'attack': 'fgsm'},
#     {'mutation': 'ns0.02', 'attack': 'fgsm'},
#     {'mutation': 'ns0.01', 'attack': 'fgsm'},
#     {'mutation': 'nai0.01', 'attack': 'fgsm'},
#     {'mutation': 'nai0.02', 'attack': 'fgsm'}
# ]

results = []
files = os.listdir('text_result/')
# for config in configs:
for file in files:
    if not file.startswith('1mnist'):
        continue
    name = file[:-4]
    # name = names.get_name(mutation=config['mutation'], attack_type=config['attack'])
    w = open('text_result/{}.txt'.format(name), 'r')
    lcr_list = [float(line) for line in w.readlines()]
    rates = []
    rates_0 = []
    rates_diff = []
    rates_yuqing_def = []
    percents = []
    tests = []
    rates_yuqing_def_norm = []
    for i in range(len(lcr_list)):
        percents.append(i)
        if i == 0:
            rates.append(0)
            rates_0.append(0)
            rates_diff.append(0)
            rates_yuqing_def.append(0)
            rates_yuqing_def_norm.append(0)
        else:
            rates.append((lcr_list[i] - lcr_list[i - 1]) / lcr_list[i - 1]) # Hadi
            rates_0.append((lcr_list[i] - lcr_list[i - 1]) / lcr_list[0]) # Armin
            rates_diff.append(lcr_list[i] - lcr_list[i - 1]) # Hadi not normalized
            rates_yuqing_def.append(lcr_list[i] - lcr_list[0]) # Yuqing
            rates_yuqing_def_norm.append((lcr_list[i] - lcr_list[0]) / lcr_list[0])  # Yuqing diff norm
    # %%
    with open('text_result/rates/{}_rates.txt'.format(name), 'w') as f:
        f.write('\n'.join([str(rate) for rate in rates]))
    with open('text_result/rates/{}_rates_0.txt'.format(name), 'w') as f:
        f.write('\n'.join([str(rate_0) for rate_0 in rates_0]))
    with open('text_result/rates/{}_rates_diff.txt'.format(name), 'w') as f:
        f.write('\n'.join([str(rate_diff) for rate_diff in rates_diff]))
    with open('text_result/rates/{}_yuqing_diff.txt'.format(name), 'w') as f:
        f.write('\n'.join([str(rate_yuqing_def) for rate_yuqing_def in rates_yuqing_def]))
    with open('text_result/rates/{}_yuqing_diff_norm.txt'.format(name), 'w') as f:
        f.write('\n'.join([str(rate_yuqing_def) for rate_yuqing_def in rates_yuqing_def_norm]))

    # %%

    indices = np.arange(0, len(rates))
    sns.lineplot(x=indices, y=moving_average(rates_0), label='(ti - ti-1)/t0')
    sns.lineplot(x=indices, y=moving_average(rates), label='(ti - ti-1)/ti-1')
    sns.lineplot(x=indices, y=moving_average(rates_diff), label='ti - ti-1')
    sns.lineplot(x=indices, y=moving_average(rates_yuqing_def), label='ti - t0')
    sns.lineplot(x=indices, y=moving_average(rates_yuqing_def_norm), label='(ti - t0)/t0')
    sns.lineplot(x=indices, y=lcr_list, label='lcr_list')
    plt.savefig('pic_result/{}_rates.png'.format(name))
    plt.clf()
    # %%
    assert len(lcr_list) == len(rates) == len(rates_0)
    results.append([name, len(lcr_list), average(lcr_list), average(rates),
                    average(rates_0), average(rates_diff),
                    round(pearsonr(percents, lcr_list)[0], 2)])

df = pd.DataFrame(results, columns=['experiment', '#', 'lcr', 'normalized_lcr', 'normalized_lcr_0', 'diff', 'pearson'])
df.to_csv('text_result/all.csv', index=False)
