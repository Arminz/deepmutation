import argparse
import os
from shutil import copyfile
import random

parser = argparse.ArgumentParser(description="The required parameters of mutation process")

parser.add_argument("--base_path", type=str, help="base path", required=True)
args = parser.parse_args()

# adv_path = './artifacts_eval/adv_samples/mnist/fgsm/2020-08-25_21:58:43/'
# adv_path = './artifacts_eval/adv_samples/mnist/bb/2020-08-30_12:57:06/'
# adv_path = './artifacts_eval/adv_samples/mnist/cw/2020-08-26_08:34:45/'
# adv_path = './artifacts_eval/adv_samples/mnist/deepfool/2020-08-25_10:46:48/'
# adv_path = './artifacts_eval/adv_samples/mnist/jsma/2020-08-25_18:48:40/'
# adv_list = [('./artifacts_eval/adv_samples/mnist/fgsm/2020-08-25_21:58:43/', 'fgsm'),
#             ('./artifacts_eval/adv_samples/mnist/bb/2020-08-30_12:57:06/', 'bb'),
#             ('./artifacts_eval/adv_samples/mnist/cw/2020-08-26_08:34:45/', 'cw'),
#             ('./artifacts_eval/adv_samples/mnist/deepfool/2020-08-25_10:46:48/', 'deepfool'),
#             ('./artifacts_eval/adv_samples/mnist/jsma/2020-08-25_18:48:40/', 'jsma')]

attack_types = ['fgsm', 'bb', 'cw', 'deepfool', 'jsma', 'test']
normal_path = os.path.join(args.base_path, "build-in-resource/pure/train-images-idx3-ubyte_folder/")
test_path = os.path.join(args.base_path, "build-in-resource/pure/t10k-images-idx3-ubyte_folder/")
adv_path_base = os.path.join(args.base_path, "artifacts_eval/adv_samples/mnist/{}/")
for attack_type in attack_types:
    print('attack_type : ', attack_type)
    for Z in range(0, 1):  # TODO: it was 10
        if attack_type == 'test':
            adv_path = test_path
        else:
            adv_path = adv_path_base.format(attack_type)
        # adv_path = adv_path_base
        adv_files = os.listdir(adv_path)
        random.shuffle(adv_files)
        normal_files = os.listdir(normal_path)
        random.shuffle(normal_files)
        for i in range(0, 101):
            print('stage i {}'.format(i))
            base_save_path = './artifacts_eval2/mixed_samples/'
            save_path = os.path.join(base_save_path, attack_type + '/r' + str(Z) + '/a' + str(i))
            if not os.path.isdir(save_path):
                os.makedirs(save_path)
            j = 1
            for file in adv_files:
                if j > i * 20:
                    break
                old_path = adv_path + '/' + file
                new_path = save_path + '/' + file
                copyfile(old_path, new_path)
                if j == i * 20:
                    break
                j = j + 1
            j = 1
            for file in normal_files:
                if j == 2001:
                    break
                old_path = normal_path + '/' + file
                new_path = save_path + '/' + file
                copyfile(old_path, new_path)
                j = j + 1
